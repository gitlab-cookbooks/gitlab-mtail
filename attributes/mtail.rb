# mtail directories
default['mtail']['home_dir'] = '/opt/prometheus/mtail'
default['mtail']['progs_dir'] = "#{node['mtail']['home_dir']}/progs"
default['mtail']['log_paths'] = [] # overridden by specialized recipes

# mtail binary
default['mtail']['bin'] = "#{node['mtail']['home_dir']}/mtail"

# mtail artifact
default['mtail']['version'] = '3.0.0-rc38'
default['mtail']['url'] = "https://github.com/google/mtail/releases/download/v#{node['mtail']['version']}/mtail_v#{node['mtail']['version']}_linux_amd64"
default['mtail']['sha256sum'] = '93090a6a924a2beb6d767972c55b6e6ec642addda1918a741b843a0a1473b981'

# mtail user / group
default['mtail']['user'] = 'root'
default['mtail']['group'] = 'root'
