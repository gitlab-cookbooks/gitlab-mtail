include_recipe 'gitlab-mtail::default'

node.default['mtail']['log_paths'] += [
  '/var/log/gitlab/postgresql/postgresql.csv',
]

cookbook_file "#{node['mtail']['progs_dir']}/postgres.mtail" do
  source 'mtail/postgres.mtail'
  notifies :restart, 'runit_service[mtail]', :delayed
end
