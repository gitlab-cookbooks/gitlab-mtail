include_recipe 'gitlab-mtail::default'

node.default['mtail']['log_paths'] << '/var/log/pubsubbeat/current'

cookbook_file File.join(node['mtail']['progs_dir'], 'pubsubbeat.mtail') do
  source 'mtail/pubsubbeat.mtail'
  owner 'root'
  group 'root'
  mode  '0644'
  notifies :restart, 'runit_service[mtail]', :delayed
end
