include_recipe 'gitlab-mtail::default'

node.default['mtail']['log_paths'] << '/var/log/gitlab/redis/current'
node.default['mtail']['log_paths'] << '/var/log/gitlab/sentinel/current'

cookbook_file File.join(node['mtail']['progs_dir'], 'redis.mtail') do
  source 'mtail/redis.mtail'
  owner 'root'
  group 'root'
  mode  '0644'
  notifies :restart, 'runit_service[mtail]', :delayed
end
