include_recipe 'gitlab-mtail::default'

node.default['mtail']['log_paths'] << '/var/log/gitlab/patroni/*.log'

cookbook_file "#{node['mtail']['progs_dir']}/patroni.mtail" do
  source 'mtail/patroni.mtail'
  notifies :restart, 'runit_service[mtail]', :delayed
end
