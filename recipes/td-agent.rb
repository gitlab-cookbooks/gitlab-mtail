include_recipe 'gitlab-mtail::default'

node.default['mtail']['log_paths'] += [
  '/var/log/td-agent/td-agent.log',
  '/var/log/fluent/fluentd.log',
]

cookbook_file File.join(node['mtail']['progs_dir'], 'td-agent.mtail') do
  source 'mtail/td-agent.mtail'
  owner 'root'
  group 'root'
  mode  '0644'
  notifies :restart, 'runit_service[mtail]', :delayed
end
