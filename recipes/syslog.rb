include_recipe 'gitlab-mtail::default'

node.default['mtail']['log_paths'] << '/var/log/syslog'

cookbook_file File.join(node['mtail']['progs_dir'], 'syslog.mtail') do
  source 'mtail/syslog.mtail'
  owner 'root'
  group 'root'
  mode  '0644'
  notifies :restart, 'runit_service[mtail]', :delayed
end
