include_recipe 'gitlab-mtail::default'

node.default['mtail']['log_paths'] << '/var/log/nginx/access.log'

cookbook_file "#{node['mtail']['progs_dir']}/grafana_nginx.mtail" do
  source 'mtail/grafana_nginx.mtail'
  owner 'root'
  group 'root'
  mode  '0644'
  notifies :restart, 'runit_service[mtail]', :delayed
end
