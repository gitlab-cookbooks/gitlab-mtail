include_recipe 'gitlab-mtail::default'

node.default['mtail']['log_paths'] += [
  '/var/log/gitlab/postgresql/postgresql.log',
  '/var/log/wal-e/wal-e_backup_push.log',
]

cookbook_file "#{node['mtail']['progs_dir']}/wale.mtail" do
  source 'mtail/wale.mtail'
  notifies :restart, 'runit_service[mtail]', :delayed
end
