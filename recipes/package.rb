include_recipe 'gitlab-mtail::default'

node.default['mtail']['log_paths'] << '/var/log/apt/term.log'

cookbook_file "#{node['mtail']['progs_dir']}/package.mtail" do
  source 'mtail/package.mtail'
  owner 'root'
  group 'root'
  mode  '0644'
  notifies :restart, 'runit_service[mtail]', :delayed
end
