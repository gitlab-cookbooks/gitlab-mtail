include_recipe 'gitlab-mtail::default'

node.default['mtail']['log_paths'] << '/var/log/nginx/*.log'

cookbook_file "#{node['mtail']['progs_dir']}/nginx.mtail" do
  source 'mtail/nginx.mtail'
  notifies :restart, 'runit_service[mtail]', :delayed
end
