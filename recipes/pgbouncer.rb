include_recipe 'gitlab-mtail::default'

node.default['mtail']['log_paths'] << '/var/log/gitlab/pgbouncer/pgbouncer.log'

cookbook_file File.join(node['mtail']['progs_dir'], 'pgbouncer.mtail') do
  source 'mtail/pgbouncer.mtail'
  owner 'root'
  group 'root'
  mode  '0644'
  notifies :restart, 'runit_service[mtail]', :delayed
end
