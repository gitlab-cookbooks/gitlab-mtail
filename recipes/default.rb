if node['platform_version'].to_i > 20
  # runit-systemd provided by runit::default is an empty transitional package in 22.04.
  # install runit-run to provide the same functionality.
  package 'runit-run'
end

include_recipe 'runit::default'

user node['prometheus']['user'] do
  system true
  shell '/bin/false'
  home "/opt/#{node['prometheus']['user']}"
  not_if node['prometheus']['user'] == 'root'
end

directory node['mtail']['home_dir'] do
  owner node['mtail']['user']
  group node['mtail']['group']
  mode '0755'
  recursive true
end

directory node['mtail']['progs_dir'] do
  owner node['mtail']['user']
  group node['mtail']['group']
  mode '0755'
end

file 'mtail-binary' do
  path node['mtail']['bin']
  action :nothing
end

remote_file node['mtail']['bin'] do
  source node['mtail']['url']
  checksum node['mtail']['sha256sum']
  owner node['mtail']['user']
  group node['mtail']['group']
  mode '0755'
  force_unlink true
  notifies :delete, 'file[mtail-binary]', :before
  notifies :restart, 'runit_service[mtail]', :delayed
end

runit_service 'mtail' do
  options(
    binary_path: node['mtail']['bin']
  )
  sv_timeout 30
  default_logger true
end
