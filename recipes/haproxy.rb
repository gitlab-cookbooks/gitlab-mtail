include_recipe 'gitlab-mtail::default'

node.default['mtail']['log_paths'] << '/var/log/haproxy.log'

cookbook_file File.join(node['mtail']['progs_dir'], 'haproxy.mtail') do
  source 'mtail/haproxy.mtail'
  owner 'root'
  group 'root'
  mode  '0644'
  notifies :restart, 'runit_service[mtail]', :delayed
end
