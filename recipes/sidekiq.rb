include_recipe 'gitlab-mtail::default'

node.default['mtail']['log_paths'] << '/var/log/gitlab/sidekiq/current'
node.default['mtail']['log_paths'] << '/var/log/gitlab/sidekiq-cluster/current'

cookbook_file "#{node['mtail']['progs_dir']}/sidekiq.mtail" do
  source 'mtail/sidekiq.mtail'
  notifies :restart, 'runit_service[mtail]', :delayed
end
