include_recipe 'gitlab-mtail::default'

node.default['mtail']['log_paths'] << '/var/log/gitlab/gitlab-rails/*.log'

cookbook_file "#{node['mtail']['progs_dir']}/rails.mtail" do
  source 'mtail/rails.mtail'
  notifies :restart, 'runit_service[mtail]', :delayed
end
