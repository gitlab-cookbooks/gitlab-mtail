include_recipe 'gitlab-mtail::default'

node.default['mtail']['log_paths'] += [
  '/var/log/wal-g/wal-g.log',
  '/var/log/wal-g/wal-g_backup_push.log',
]

cookbook_file "#{node['mtail']['progs_dir']}/walg.mtail" do
  source 'mtail/walg.mtail'
  notifies :restart, 'runit_service[mtail]', :delayed
end
