include_recipe 'gitlab-mtail::default'

node.default['mtail']['log_paths'] << '/var/log/gitlab/gitlab-shell/gitlab-shell.log'

cookbook_file File.join(node['mtail']['progs_dir'], 'gitlab-shell.mtail') do
  source 'mtail/gitlab-shell.mtail'
  owner 'root'
  group 'root'
  mode  '0644'
  notifies :restart, 'runit_service[mtail]', :delayed
end
