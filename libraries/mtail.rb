module Gitlab
  module Mtail
    def self.format_log_paths(paths)
      case paths
      when Array
        paths.join(',')
      when String
        paths
      else
        ''
      end
    end
  end
end
