name             'gitlab-mtail'
maintainer       'GitLab'
maintainer_email 'ops-contact@gitlab.com'
license          'All rights reserved'
description      'Installs/Configures gitlab-mtail'
version          '2.6.0'

depends 'runit'
