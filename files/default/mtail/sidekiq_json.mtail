# Match only sidekiq/sidekiq-cluster current logs.
getfilename() !~ /(sidekiq|sidekiq-cluster)\/current$/ {
  stop
}

# Sidekiq json log format metrics.
counter sidekiq_log_lines_total by severity

# ERROR metrics.
counter sidekiq_remote_remove_failed_total

# Normal sidekiq JSON log lines.
/^{"severity":"(?P<severity>[A-Z]+)/ {
  sidekiq_log_lines_total[$severity]++

  $severity == "ERROR" {
    /"message":"Could not remove remote/ {
      sidekiq_remote_remove_failed_total++
    }
  }
}

# Sidekiq master log lines.
gauge sidekiq_cluster_start_time_seconds
gauge sidekiq_cluster_workers

counter sidekiq_cluster_shutdowns_total

/^(?P<master_time>\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2})\.\d+Z \d+ TID-.* (?P<master_severity>[A-Z]+): / {
  strptime($master_time, "2006-01-02T15:04:05")

  # Cluster startup.
  /Starting cluster with (?P<worker_count>\d+) processes/ {
    sidekiq_cluster_start_time_seconds = timestamp()
    sidekiq_cluster_workers = $worker_count
  }
  # Cluster shutdown.
  /INFO: A worker terminated, shutting down the cluster/ {
    sidekiq_cluster_shutdowns_total++
  }
}

# Other log messages.
counter sidekiq_bind_errors_total
/Address already in use - bind/ {
  sidekiq_bind_errors_total++
}
