# Match only gitlab-shell/gitlab-shell.log.
getfilename() !~ /gitlab-shell\/gitlab-shell\.log$/ {
  stop
}

# gitlab-shell JSON log parsing

# HTTP metrics
histogram gitlab_shell_request_seconds buckets 0.0, 0.01, 0.05, 0.1, 0.25, 0.5, 1.0, 5.0, 10.0  by method, status
# Command metrics
counter gitlab_shell_git_commands_total by command, gl_key_type, git_protocol

# HTTP log lines
/^{.*}$/ {
  # Parse HTTP request method.
  /"method":"(?P<method>[A-Z]+)"/ {
    # Parse the duration in milliseconds.
    /"duration_ms":(?P<duration_ms>\d+)/ {
      # Parse the status code.
      /"status":(?P<status>\d+)/ { 
        gitlab_shell_request_seconds[$method][$status] = $duration_ms / 1000.0
      }
    }
  }

  # Parse commands.
  /"command":"(?P<command>[a-z-]+)"/ {
    /"gl_key_type":"(?P<gl_key_type>[a-z_]+)"/ {
      /"git_protocol":"((?P<git_protocol>version=\d+))?"/ {
        $git_protocol == "" {
          gitlab_shell_git_commands_total[$command][$gl_key_type]["version=1"]++
        } else {
          gitlab_shell_git_commands_total[$command][$gl_key_type][$git_protocol]++
        }
      }
    }
  }
}
