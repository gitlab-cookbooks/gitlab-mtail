require 'spec_helper'

describe 'gitlab-mtail::postgres' do
  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
      end.converge(described_recipe)
    end

    it 'creates the mtail dir in the configured location' do
      expect(chef_run).to create_directory('/opt/prometheus/mtail/progs')
    end

    it 'creates the mtail postgres mtail script' do
      expect(chef_run).to create_cookbook_file('/opt/prometheus/mtail/progs/postgres.mtail')
    end
  end
end
