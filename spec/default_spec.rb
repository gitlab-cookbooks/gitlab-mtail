require 'spec_helper'

describe 'gitlab-mtail::default' do
  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
      end.converge(described_recipe)
    end

    it 'creates a prometheus user and group' do
      expect(chef_run).to create_user('prometheus').with(
        system: true,
        shell: '/bin/false'
      )
    end

    it 'creates mtail home dir' do
      expect(chef_run).to create_directory('/opt/prometheus/mtail')
    end

    it 'creates the mtail dir in the configured location' do
      expect(chef_run).to create_directory('/opt/prometheus/mtail/progs')
    end

    it 'creates the runit service' do
      expect(chef_run).to enable_runit_service('mtail')
    end
  end
end
