require 'spec_helper'

describe 'gitlab-mtail::gitlab-shell' do
  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
      end.converge(described_recipe)
    end

    it 'creates the mtail dir in the configured location' do
      expect(chef_run).to create_directory('/opt/prometheus/mtail/progs')
    end

    it 'creates the mtail gitlab-shell  script' do
      expect(chef_run).to create_cookbook_file('/opt/prometheus/mtail/progs/gitlab-shell.mtail').with(
        owner: 'root',
        group: 'root',
        mode: '0644'
      )
    end
  end
end
